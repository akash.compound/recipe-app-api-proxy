FROM nginxinc/nginx-unprivileged:1-alpine
LABEL maintainer="aporwal191@gmail.com"


COPY ./defualt.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uwsgi_params /etc/nginx/uwsgi_paramas

ENV LISTEN_PORT = 8000
ENV APP_HOST = app
ENV APP_PORT = 9000


USER root

RUN mkdir -p /vol/static
RUN chmod 755 /vol/static
RUN touch /etc/nginx/conf.d/defualt.conf
RUN chown nginx:nginx /etc/nginx/conf.d/default.conf

COPY ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

USER nginx

CMD ["/entrypoint.sh"]